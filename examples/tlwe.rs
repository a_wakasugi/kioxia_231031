use TFHE_encrypt::*;

pub fn main() {
    let a: f32;
    let b: f32;
    let cleartext1: f32;
    let cleartext2: f32;
    let discretized_torus_bit: usize;
    let computation_accuracy_bit: usize;
    let poly_size: usize;
    let lwe_encryption_sample_num: usize;
    let e: Vec<u64>;
    let lwe_sample: Vec<Vec<u64>>;

    (
        a,
        b,
        cleartext1, 
        cleartext2, 
        discretized_torus_bit,
        computation_accuracy_bit,
        lwe_encryption_sample_num,
        poly_size,
        e, 
        lwe_sample,
    ) = consts::fixed_params();

    let seckey: Vec<Vec<u64>> =
        tlwe::tlwe_secret_key_generation::tlwe_secret_key_generation(lwe_encryption_sample_num, discretized_torus_bit);

    let ptxt1 =
        tlwe::tlwe_encode::encode(cleartext1, a, b, computation_accuracy_bit, discretized_torus_bit);
    
    let ctxt1 = tlwe::tlwe_encrypt::tlwe_encrypt(
        seckey.clone(),
        e.clone(), 
        lwe_sample.clone(), 
        ptxt1.clone(),
        lwe_encryption_sample_num,
        discretized_torus_bit,
    );

    let serialized_ctxt1 = tlwe::tlwe_serialize::serialize(ctxt1.clone());
    
    let deserialized_ctxt1 = tlwe::tlwe_serialize::deserialize(serialized_ctxt1.clone());
    
    let decrypted_ctxt1 = tlwe::tlwe_encrypt::tlwe_decrypt(
        seckey.clone(),
        deserialized_ctxt1.clone(),
        computation_accuracy_bit,
        discretized_torus_bit,
        lwe_encryption_sample_num,
    );
    
    let decoded_decrypted_ctxt1 = tlwe::tlwe_encode::decode(
        decrypted_ctxt1.clone(),
        a,
        b,
        computation_accuracy_bit,
        discretized_torus_bit,
    );
}