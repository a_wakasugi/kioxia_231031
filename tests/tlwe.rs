use TFHE_encrypt::*;

const TEST_NUM: usize = 100;

fn init() -> (f32, f32, f32, f32, usize, usize, usize, usize, Vec<u64>, Vec<Vec<u64>>) {
    let a: f32;
    let b: f32;
    let cleartext1: f32;
    let cleartext2: f32; 
    let discretized_torus_bit: usize;
    let computation_accuracy_bit: usize;
    let lwe_encryption_sample_num: usize;
    let poly_size: usize;
    let e: Vec<u64>;
    let lwe_sample: Vec<Vec<u64>>;

    (
        a,
        b,
        cleartext1, 
        cleartext2, 
        discretized_torus_bit,
        computation_accuracy_bit,
        lwe_encryption_sample_num,
        poly_size,
        e, 
        lwe_sample,
    ) = consts::fixed_params();

    return (
        a,
        b,
        cleartext1, 
        cleartext2, 
        discretized_torus_bit,
        computation_accuracy_bit,
        lwe_encryption_sample_num,
        poly_size,
        e, 
        lwe_sample, 
    );
}


fn relative_equal(a: f32, b: f32) -> bool {
    // pass if a and b are equal within 1e-2
    if a < b + 1e-2 && b - 1e-2 < a {
        return true;
    } else {
        return false;
    }
}


#[test]
fn encode_decode() {
    let (a, b, cleartext1, cleartext2, discretized_torus_bit, computation_accuracy_bit, lwe_encryption_sample_num, poly_size, e, lwe_sample) = init();

    // repeat 100 times
    for _ in 0..TEST_NUM {
        let ptxt1 = tlwe::tlwe_encode::encode(
            cleartext1,
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        let decoded1 = tlwe::tlwe_encode::decode(
            ptxt1.clone(),
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        assert!(relative_equal(cleartext1, decoded1));
    }
}


#[test]
fn encode_encrypt_decrypt_decode() {
    let (a, b, cleartext1, cleartext2, discretized_torus_bit, computation_accuracy_bit, lwe_encryption_sample_num, poly_size, e, lwe_sample) = init();

    for _ in 0..TEST_NUM {
        let seckey: Vec<Vec<u64>> =
            tlwe::tlwe_secret_key_generation::tlwe_secret_key_generation(lwe_encryption_sample_num, discretized_torus_bit);

        let ptxt1 = tlwe::tlwe_encode::encode(
            cleartext1,
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        let ctxt1 = tlwe::tlwe_encrypt::tlwe_encrypt(
            seckey.clone(),
            e.clone(), 
            lwe_sample.clone(), 
            ptxt1.clone(),
            lwe_encryption_sample_num,
            discretized_torus_bit,
        );
        let decrypted_ctxt2 = tlwe::tlwe_encrypt::tlwe_decrypt(
            seckey.clone(),
            ctxt1.clone(),
            computation_accuracy_bit,
            discretized_torus_bit,
            lwe_encryption_sample_num,
        );
        let decoded1 = tlwe::tlwe_encode::decode(
            decrypted_ctxt2.clone(),
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        assert!(relative_equal(cleartext1, decoded1));
    }
}

#[test]
fn serialize_deserialize_ctxt() {
    let (a, b, cleartext1, clearnum2, discretized_torus_bit, computation_accuracy_bit, lwe_encryption_sample_num, poly_size, e, lwe_sample) = init();

    for _ in 0..TEST_NUM {
        let seckey: Vec<Vec<u64>> =
            tlwe::tlwe_secret_key_generation::tlwe_secret_key_generation(lwe_encryption_sample_num, discretized_torus_bit);

        let ptxt1 = tlwe::tlwe_encode::encode(
            cleartext1,
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        let ctxt1 = tlwe::tlwe_encrypt::tlwe_encrypt(
            seckey.clone(),
            e.clone(), 
            lwe_sample.clone(), 
            ptxt1.clone(),
            lwe_encryption_sample_num,
            discretized_torus_bit,
        );
        let serialized_ctxt1 = tlwe::tlwe_serialize::serialize(ctxt1.clone());
        let deserialized_ctxt1 = tlwe::tlwe_serialize::deserialize(serialized_ctxt1.clone());
        assert_eq!(ctxt1, deserialized_ctxt1);
    }
}

#[test]
fn add_ptxt() {
    let (a, b, cleartext1, cleartext2, discretized_torus_bit, computation_accuracy_bit, lwe_encryption_sample_num, poly_size, e, lwe_sample) = init();

    for _ in 0..TEST_NUM {
        let ptxt1 = tlwe::tlwe_encode::encode(
            cleartext1,
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        let ptxt2 = tlwe::tlwe_encode::encode(
            cleartext2,
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        let added_ptxt = tlwe::tlwe_operation::tlwe_plaintext_operation::tlwe_plaintext_add(
            ptxt1.clone(),
            ptxt2.clone(),
            discretized_torus_bit
        );
        let decoded_added_ptxt = tlwe::tlwe_encode::decode(
            added_ptxt.clone(),
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        assert!(relative_equal(basic_function::cleartext_range_check::cleartext_range_check(cleartext1 + cleartext2, a, b), decoded_added_ptxt));
    }
}

#[test]
fn sub_ptxt() {
    let (a, b, cleartext1, cleartext2, discretized_torus_bit, computation_accuracy_bit, lwe_encryption_sample_num, poly_size, e, lwe_sample) = init();

    for _ in 0..TEST_NUM {
        let ptxt1 = tlwe::tlwe_encode::encode(
            cleartext1,
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        let ptxt2 = tlwe::tlwe_encode::encode(
            cleartext2,
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        let subed_ptxt = tlwe::tlwe_operation::tlwe_plaintext_operation::tlwe_plaintext_sub(
            ptxt1.clone(),
            ptxt2.clone(),
            discretized_torus_bit
        );
        let decoded_subed_ptxt = tlwe::tlwe_encode::decode(
            subed_ptxt.clone(),
            a,
            b,
            computation_accuracy_bit,
            discretized_torus_bit,
        );
        assert!(relative_equal(basic_function::cleartext_range_check::cleartext_range_check(cleartext1 - cleartext2, a, b), decoded_subed_ptxt));
    }
}