pub fn num_to_bin_list(
    mut num: u64, 
    list_size: usize
) -> Vec<u64> {
    // convert the u64-type integer into the 48-bit vector using by a two expansion
    let mut rev_bin_vec: Vec<u64> = Vec::new();

    while num != 0{
        rev_bin_vec.push(num % 2);
        num /= 2;
    }

    let rev_bin_vec_length = rev_bin_vec.len();
    for _i in 0..(list_size-rev_bin_vec_length){
        rev_bin_vec.push(0);
    }

    let mut bin_vec: Vec<u64> = Vec::new();
    for i in 0..list_size{
        bin_vec.push(rev_bin_vec[list_size - 1 - i]);
    }

    return bin_vec;
}


pub fn bin_list_to_num(
    bin_vec: Vec<u64>, 
    list_size: usize
) -> u64 {
    // convert the 48-bit vector into the u64-type integer
    let mut num: u64 = 0;

    for i in 0..list_size{
        if bin_vec[list_size-1-i] == 1 {
            num += 2_u64.pow(i.try_into().unwrap()) as u64;
        }
    }

    return num;
}