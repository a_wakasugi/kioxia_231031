pub fn cleartext_range_check(
    clearnum: f32, 
    a: f32, 
    b: f32
) -> f32 {
    // adjust cleartext values in [a, b]
    let clear_range_width: i32 = (b - a) as i32;
    
    let mut cleartext = clearnum;
    if cleartext > b {
        while cleartext > b {
            cleartext -= clear_range_width as f32;
        }
    }
    else if cleartext < a {
        while cleartext < a {
            cleartext += clear_range_width as f32;
        }
    }
    
    return cleartext;
}