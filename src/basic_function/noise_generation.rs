// This script is not used (reference script)

use rand::distributions::{Normal, Distribution};

pub fn noise_generation(stddev: f32, discretized_torus_bit: usize) -> Vec<u8> {
    // noise samples from a normal distribution N(0, stddev)
    // noise is represented for u64 using by the two expansion
    let mut rng = rand::thread_rng();
    let mean: f32 = 0.0;
    let mut noise_flag = false;
    let noise_vec: Vec<u8> = Vec::new();

    // if noise is 0, repeat samples
    while noise_flag == false {
        let normal = Normal::new(mean.into(), stddev.into());
        let mut noise = normal.sample(&mut rng);

        let mut noise_vec: Vec<u8> = Vec::new();
        if noise >= 0.0 {
            noise_vec.push(0);
        }
        else {
            noise_vec.push(1);
        }

        let mut two_power = 0.5;
        for _ in 0..(discretized_torus_bit - 1) {
            if noise >= two_power {
                noise_vec.push(1);
                noise -= two_power;
                noise_flag = true;
            }
            else {
                noise_vec.push(0);
            }
            two_power /= 2.0;
        }

        if noise_flag == false {
            noise_vec == Vec::new();
        }
    }

    return noise_vec;
}
