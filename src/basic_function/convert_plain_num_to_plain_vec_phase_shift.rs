// This script is a reference script(not used)

pub fn ptxt_num_to_vec(ptxt_num: i32, discretized_torus_bit: usize) -> Vec<u64> {
    let mut ptxt_vec: Vec<u64> = Vec::new();

    if ptxt_num < 0 {
        ptxt_vec.push(1);
    }
    else {
        ptxt_vec.push(0);
    }

    let mut abs_ptxt_num = ptxt_num.abs();
    let mut two_power = 2_i32.pow((discretized_torus_bit as i32 - 2).try_into().unwrap());

    for _ in 1..discretized_torus_bit {
        if abs_ptxt_num < two_power {
            ptxt_vec.push(0);
        }
        else {
            ptxt_vec.push(1);
            abs_ptxt_num -= two_power;
        }
        two_power /= 2;
    }

    if ptxt_num < 0 {
        for i in 1..discretized_torus_bit {
            ptxt_vec[discretized_torus_bit - i] = 1 - ptxt_vec[discretized_torus_bit - i];
        }
        ptxt_vec[discretized_torus_bit - 1] += 1;
        for i in 1..discretized_torus_bit {
            if ptxt_vec[discretized_torus_bit - i] == 2 {
                ptxt_vec[discretized_torus_bit - i] = 0;
                ptxt_vec[discretized_torus_bit - i - 1] += 1;
            }
            else {
                break;
            }
        }
    }

    return ptxt_vec;
}

pub fn ptxt_vec_to_num(ptxt_vec: Vec<u64>, discretized_torus_bit: usize) -> i32 {
    let mut ptxt_num: i32 = 0;
    for i in 1..(discretized_torus_bit) {
        if ptxt_vec[discretized_torus_bit - i] == 1 {
            ptxt_num += 2_i32.pow(((i - 1) as i32).try_into().unwrap());
        }
    }
    if ptxt_vec[0] == 1 {
        ptxt_num *= -1;
        ptxt_num += 2_i32.pow(((discretized_torus_bit - 2) as i32).try_into().unwrap());
        ptxt_num += 2_i32.pow(((discretized_torus_bit - 2) as i32).try_into().unwrap());
        ptxt_num *= -1;
    }

    return ptxt_num;
}