pub fn serialize(
    ctxt: Vec<Vec<u64>>
) -> Vec<u8> {
    // ctxt (2-dim) size is ((lwe_encryption_sample_num + 1), discretized_torus_bit)
    // → serialize_ctxt (1-dim) size is ((lwe_encryption_sample_num + 1) * 6 * discretized_torus_bit)
    let mut serialize_ctxt: Vec<u8> = Vec::new();

    // loop through the ctxt and take 8 bits at a time and convert them to u8
    for i in 0..ctxt.len(){
        for j in 0..6{
            let mut serialize_num: u8 = 0;
            for k in 0..8{
                let index = 8 * j + k;
                serialize_num += 2_u8.pow(7 - k) * (ctxt[i as usize][index as usize] as u8);
            }
            serialize_ctxt.push(serialize_num);
        }
    }

    return serialize_ctxt;
}

pub fn deserialize(
    serialize_ctxt: Vec<u8>
) -> Vec<Vec<u64>> {
    // serialize_ctxt (1-dim) size is ((lwe_encryption_sample_num + 1) * 6 * discretized_torus_bit)
    // →deserialize_ctxt (2-dim) size is ((lwe_encryption_sample_num + 1), discretized_torus_bit)
    let mut deserialize_ctxt: Vec<Vec<u64>> = Vec::new();

    let ctxt_length = serialize_ctxt.len();

    // loop through the serialize_ctxt and take 4 bytes at a time and convert them to u64
    for i in 0..ctxt_length / 6{
        let mut deserialize_ctxt_i: Vec<u64> = Vec::new();
        for j in 0..6{
            let mut serialize_ctxt_num = serialize_ctxt[(6 * i + j) as usize];
            for k in 0..8{
                if serialize_ctxt_num < 2_u8.pow(7 - k){
                    deserialize_ctxt_i.push(0);
                }
                else{
                    deserialize_ctxt_i.push(1);
                    serialize_ctxt_num -= 2_u8.pow(7 - k);
                }
            }
        }
        deserialize_ctxt.push(deserialize_ctxt_i);
    }

    return deserialize_ctxt;
}