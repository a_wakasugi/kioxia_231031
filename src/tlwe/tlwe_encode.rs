use super::super::basic_function::cleartext_range_check::*;
use super::super::basic_function::convert_num_to_bin_list::*;

pub fn encode(
    clearnum: f32, 
    a: f32, 
    b: f32, 
    computation_accuracy_bit: usize, 
    discretized_torus_bit: usize
) -> Vec<u64> {
    // without a phase shift
    // clearnum: f32 → ptxt_num: u64 → ptxt_vec: Vec<u64>
    let cleartext = cleartext_range_check(clearnum, a, b);
    let torus_num = (cleartext - a) / (b - a);
    let clear_num_in_ptxt = torus_num * 2.0_f32.powf((computation_accuracy_bit as f32).try_into().unwrap());
    let int_clear_num_in_ptxt: u64 = clear_num_in_ptxt.round() as u64;
    let ptxt_num: u64 = int_clear_num_in_ptxt * 2_u64.pow((discretized_torus_bit as u64 - computation_accuracy_bit as u64 - 1).try_into().unwrap());

    let ptxt_vec: Vec<u64> = num_to_bin_list(ptxt_num, discretized_torus_bit);

    return ptxt_vec;
}


pub fn decode(
    ptxt: Vec<u64>, 
    a: f32, 
    b: f32, 
    computation_accuracy_bit: usize, 
    discretized_torus_bit: usize
) -> f32 {
    // without a phase shift
    // ptxt: Vec<u64> → ptxt_num: u64 → dec_ptxt: f32
    let ptxt_num: u64 = bin_list_to_num(ptxt.clone(), discretized_torus_bit);

    let int_clear_num_in_dec_ptxt = ptxt_num / 2_u64.pow((discretized_torus_bit as u64 - computation_accuracy_bit as u64).try_into().unwrap()) as u64;
    let mut torus_num = int_clear_num_in_dec_ptxt as f32;
    
    for _ in 1..(computation_accuracy_bit) {
        torus_num /= 2.0;
    }

    let mut dec_ptxt = torus_num * (b - a) + a;
    dec_ptxt = cleartext_range_check(dec_ptxt, a, b);

    return dec_ptxt;
}