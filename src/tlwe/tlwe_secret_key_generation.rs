use super::super::basic_function::convert_num_to_bin_list::*;
use rand::*;

// generate the secret key used in LWE cryptosystem
pub fn tlwe_secret_key_generation(
    lwe_encryption_sample_num: usize, 
    discretized_torus_bit: usize
) -> Vec<Vec<u64>> {
    // seckey (2-dim) size is ((discretized_torus_bit), lwe_encryption_sample_num)
    let mut rng = rand::thread_rng();
    let mut tlwe_secret_key: Vec<Vec<u64>> = Vec::new();
    for _i in 0..lwe_encryption_sample_num{
        tlwe_secret_key.push(num_to_bin_list(rng.gen_range(0, 2), discretized_torus_bit));
    }
    return tlwe_secret_key;

}