use super::super::super::basic_function::convert_num_to_bin_list::*;

pub fn tlwe_plaintext_add(
    ptxt1: Vec<u64>, 
    ptxt2: Vec<u64>, 
    discretized_torus_bit: usize
) -> Vec<u64> {
    let ptxt_len = ptxt1.len();
    let ptxt_num1 = bin_list_to_num(ptxt1, ptxt_len);
    let ptxt_num2 = bin_list_to_num(ptxt2, ptxt_len);

    let phase_shift_num: u64 = 2_u64.pow(((ptxt_len - 2) as u64).try_into().unwrap());
    
    let mut ptxt_num = ptxt_num1.wrapping_add(ptxt_num2);
    ptxt_num = ptxt_num.wrapping_sub(phase_shift_num);

    ptxt_num %= 2_u64.pow((discretized_torus_bit as u64).try_into().unwrap());

    let ptxt = num_to_bin_list(ptxt_num, ptxt_len);

    return ptxt;
}

pub fn tlwe_plaintext_sub(
    ptxt1: Vec<u64>, 
    ptxt2: Vec<u64>, 
    discretized_torus_bit: usize
) -> Vec<u64> {
    let ptxt_len = ptxt1.len();
    let ptxt_num1 = bin_list_to_num(ptxt1, ptxt_len);
    let ptxt_num2 = bin_list_to_num(ptxt2, ptxt_len);

    let phase_shift_num: u64 = 2_u64.pow(((ptxt_len - 2) as u64).try_into().unwrap());
    
    let mut ptxt_num = ptxt_num1.wrapping_sub(ptxt_num2);
    ptxt_num %= 2_u64.pow((discretized_torus_bit as u64).try_into().unwrap());

    ptxt_num = ptxt_num.wrapping_add(phase_shift_num);
    ptxt_num %= 2_u64.pow((discretized_torus_bit as u64).try_into().unwrap());

    let ptxt = num_to_bin_list(ptxt_num, ptxt_len);

    return ptxt;
}