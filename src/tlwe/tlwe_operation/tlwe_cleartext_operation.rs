use super::super::super::basic_function::cleartext_range_check::*;

pub fn cleartext_add(
    clearnum1: f32, 
    clearnum2: f32, 
    a: f32, 
    b: f32
) -> f32 {
    let cleartext1 = cleartext_range_check(clearnum1, a, b);
    let cleartext2 = cleartext_range_check(clearnum2, a, b);

    let clearnum = cleartext1 + cleartext2;

    let cleartext = cleartext_range_check(clearnum, a, b);

    return cleartext;
}

pub fn cleartext_sub(
    clearnum1: f32, 
    clearnum2: f32, 
    a: f32, 
    b: f32
) -> f32 {
    let cleartext1 = cleartext_range_check(clearnum1, a, b);
    let cleartext2 = cleartext_range_check(clearnum2, a, b);

    let clearnum = cleartext1 - cleartext2;

    let cleartext = cleartext_range_check(clearnum, a, b);

    return cleartext;
}