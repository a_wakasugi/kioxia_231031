use super::super::basic_function::convert_num_to_bin_list::*;

// use super::super::basic_function::noise_generation::*;
// use rand::*;


pub fn tlwe_encrypt(
    seckey: Vec<Vec<u64>>, 
    e: Vec<u64>, 
    lwe_sample: Vec<Vec<u64>>, 
    ptxt: Vec<u64>, 
    lwe_encryption_sample_num: usize, 
    discretized_torus_bit: usize
) -> Vec<Vec<u64>> {
    // ctxt (2-dim) size is ((lwe_encryption_sample_num + 1), discretized_torus_bit)
    let mut ctxt_n_num: u64 = 0;

    // compute the inner product <seckey, lwe_sample>
    for i in 0..=(lwe_encryption_sample_num-1){
        if seckey[i] == num_to_bin_list(1, discretized_torus_bit){
            let lwe_sample_i_num: u64 = bin_list_to_num(lwe_sample[i].clone(), discretized_torus_bit);
            ctxt_n_num = ctxt_n_num.wrapping_add(lwe_sample_i_num.clone());
        }
    }
    
    // compute <seckey, lwe_sample> + ptxt + e
    let ptxt_num: u64 = bin_list_to_num(ptxt.clone(), discretized_torus_bit);
    let e_num: u64 = bin_list_to_num(e.clone(), discretized_torus_bit);
    ctxt_n_num = ctxt_n_num.wrapping_add(ptxt_num.clone());
    ctxt_n_num = ctxt_n_num.wrapping_add(e_num.clone());
    ctxt_n_num %= 2_u64.pow((discretized_torus_bit as u64).try_into().unwrap());
    let ctxt_n = num_to_bin_list(ctxt_n_num.clone(), discretized_torus_bit);

    let mut ctxt: Vec<Vec<u64>> = lwe_sample;
    ctxt.push(ctxt_n.clone());

    return ctxt;
}


pub fn upper(
    mut phase: Vec<u64>, 
    computation_accuracy_bit: usize, 
    discretized_torus_bit: usize
) -> Vec<u64> {
    // delete the noise part for the phase
    for j in 1..(discretized_torus_bit-computation_accuracy_bit){
        phase[discretized_torus_bit-j] = 0;
    }
    return phase;
}


pub fn tlwe_decrypt(
    seckey: Vec<Vec<u64>>, 
    ctxt: Vec<Vec<u64>>, 
    computation_accuracy_bit: usize, 
    discretized_torus_bit: usize, 
    lwe_encryption_sample_num: usize
) -> Vec<u64> {
    let mut phase_num: u64 = bin_list_to_num(ctxt[lwe_encryption_sample_num].clone(), discretized_torus_bit);
    phase_num %= 2_u64.pow((discretized_torus_bit as u64).try_into().unwrap());
    
    // compute the phase
    // phase = ctxt - <seckey, lwe_sample>
    for i in 0..=(lwe_encryption_sample_num-1){
        if seckey[i] == num_to_bin_list(1, discretized_torus_bit){
            let ctxt_i_num: u64 = bin_list_to_num(ctxt[i].clone(), discretized_torus_bit);
            phase_num = phase_num.wrapping_sub(ctxt_i_num.clone());
            phase_num %= 2_u64.pow((discretized_torus_bit as u64).try_into().unwrap());
        }
    }

    let phase = num_to_bin_list(phase_num.clone(), discretized_torus_bit);
    let ptxt: Vec<u64> = upper(phase, computation_accuracy_bit, discretized_torus_bit);

    return ptxt;
}