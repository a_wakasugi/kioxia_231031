// This script is a reference script(not used)

use super::super::basic_function::cleartext_range_check::*;
use super::super::basic_function::convert_plain_num_to_plain_vec::*;

pub fn encode(clearnum: f32, a: f32, b: f32, computation_accuracy_bit: usize, discretized_torus_bit: usize) -> Vec<u64> {
    let cleartext = cleartext_range_check(clearnum, a, b);
    let torus_num = (cleartext - a) / (b - a);
    let clear_num_in_ptxt = torus_num * 2.0_f32.powi((computation_accuracy_bit as i32 - 1).try_into().unwrap());
    let int_clear_num_in_ptxt: u64 = clear_num_in_ptxt.round() as u64;
    let ptxt_num_u64 = int_clear_num_in_ptxt * 2_u64.pow((discretized_torus_bit as u64 - computation_accuracy_bit as u64 + 1).try_into().unwrap());
    let mut ptxt_num: i32;
    if ptxt_num_u64 < 2_u64.pow((discretized_torus_bit as u64 - 1).try_into().unwrap()) {
        ptxt_num = ptxt_num_u64 as i32;
        ptxt_num -= 2_i32.pow((discretized_torus_bit as i32 - 2).try_into().unwrap()) as i32;
        ptxt_num -= 2_i32.pow((discretized_torus_bit as i32 - 2).try_into().unwrap()) as i32;
    }
    else {
        ptxt_num = (ptxt_num_u64 - 2_u64.pow((discretized_torus_bit as u64 - 1).try_into().unwrap())) as i32;
    }

    let ptxt_vec: Vec<u64> = ptxt_num_to_vec(ptxt_num, discretized_torus_bit);

    return ptxt_vec;
}


pub fn decode(ptxt: Vec<u64>, a: f32, b: f32, computation_accuracy_bit: usize, discretized_torus_bit: usize) -> f32 {
    let mut dec_ptxt_num_i32 = ptxt_vec_to_num(ptxt.clone(), discretized_torus_bit);

    let mut dec_ptxt_num_u64: u64;
    if dec_ptxt_num_i32 < 0 {
        dec_ptxt_num_i32 += 2_i32.pow((discretized_torus_bit as i32 - 2).try_into().unwrap()) as i32;
        dec_ptxt_num_i32 += 2_i32.pow((discretized_torus_bit as i32 - 2).try_into().unwrap()) as i32;
        dec_ptxt_num_u64 = dec_ptxt_num_i32 as u64;
    }
    else {
        dec_ptxt_num_u64 = dec_ptxt_num_i32 as u64;
        dec_ptxt_num_u64 += 2_u64.pow((discretized_torus_bit as u64 - 1).try_into().unwrap()) as u64;
    }

    let int_clear_num_in_dec_ptxt = dec_ptxt_num_u64 / 2_u64.pow((discretized_torus_bit as u64 - computation_accuracy_bit as u64 + 1).try_into().unwrap()) as u64;
    let mut torus_num = int_clear_num_in_dec_ptxt as f32;
    
    for _ in 0..(computation_accuracy_bit - 1) {
        torus_num /= 2.0;
    }

    if dec_ptxt_num_i32 < 0 {
        torus_num = 1.0 - torus_num;
    }

    let dec_ptxt = torus_num * (b - a) + a;

    return dec_ptxt;
}